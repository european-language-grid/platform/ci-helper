# Ci Helper

Default gitlab-ci.yaml for use with dedicated ELG runners. Containes runner specific configuration that will be updated alongside the installed runners as required. 

If you want to use ELG provided runners:
 - disable shared runners on your repo/settings/ci
 - use the .gitlab-ci found in this repo instead of a custom one
 - this will include the default settings from this repo, those will be updated as needed and your build will fetch new versions automatically
 - the runners are configured under the ELG group, so are inherited by all groups and repos

The provided config tags images according to the branch name (gitlab standard).

## License
MIT License

## Project status
This is an early version, will be generalized as needed.

