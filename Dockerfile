# Example Dockerfile with separate build and run images

#Build image
FROM busybox
WORKDIR /build
COPY * /build/

#Runtime image
FROM alpine
#copy from build stage
COPY --from=0 /build/  /foo

EXPOSE 8080
ENTRYPOINT ["your program"]
